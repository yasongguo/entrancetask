// import HyperNode from '../../SceneGraph/HyperNode'
import { BoxBufferGeometry, MeshBasicMaterial, Object3D,
  // WebGLRenderTarget, PerspectiveCamera, WebGLRenderer, Scene, SphereBufferGeometry,
  // LinearFilter, NearestFilter,
  Mesh, DoubleSide } from 'three'
// import { MAJOR_AXIS } from '../../../Math/PlanetConstants'
const MAJOR_AXIS = 6378137.0

import { pointStarTexture } from './SkyTexture'

// import OfflineRenderer from '../../../Material/offlineRenderer'

// const BOX_SIZE = MAJOR_AXIS * 10000 // < (far - 3) / 1.732 * 2

// camera.position.z = 300

class SkyBox extends Object3D {
  constructor ({
    parent
  }) {
    super()
    parent.add(this)
    this.name = 'SkyBox'
    console.log(this)

    this.addBox({
      BOXSIZE: MAJOR_AXIS * 20, // may need to adjust PlanetControl::__updateNearFar() far plane
      radius: 200,
      NSTARS: 10000,
      baseColor: { h: 0.4, s: 0.2, l: 0.6 },
      transparent: false,
      opacity: 1
    })

    this.addBox({
      BOXSIZE: MAJOR_AXIS * 6,
      radius: 150,
      NSTARS: 400,
      baseColor: { h: 0.2, s: 0.1, l: 1 },
      transparent: true,
      opacity: 0
    })
  }

  addBox ({
    BOXSIZE = MAJOR_AXIS * 60,
    radius = 200,
    NSTARS = 10000,
    baseColor,
    // showPoints = false,
    transparent = false,
    opacity = 1
  } = {}) {
    let material
    // if (showPoints) {
    let ptTex = pointStarTexture({
      NSTARS,
      radius,
      baseColor,
      transparent,
      opacity
    })
    material = new MeshBasicMaterial({ side: DoubleSide, map: ptTex, transparent: true })
    // } else {
      // material = new MeshBasicMaterial({ color: 0xff0000, side: DoubleSide, wireframe: true, transparent: true, opacity: 0.3 })
    // }

    let geometry = new BoxBufferGeometry(BOXSIZE, BOXSIZE, BOXSIZE)

    let cube = new Mesh(geometry, material)
    console.log(cube)
    // this.cube.scale.set(BOXSIZE, BOXSIZE, BOXSIZE)

    this.add(cube)
    // this.sceneNode.add(this.cubeNear)
  }
}

export default SkyBox
