/**
 * Throttle function: limiting the rate at which we execute a function.
 *
 * @see {@line https://medium.com/@_jh3y/throttling-and-debouncing-in-javascript-b01cad5c8edf|throttle}
 * @param {function} func
 * @param {number} limit
 */
const throttle = (func, limit) => {
  let inThrottle
  let lastFunc
  let lastRan
  return function () {
    const context = this
    const args = arguments
    if (!inThrottle) {
      func.apply(context, args)
      lastRan = Date.now()
      inThrottle = true
    } else {
      clearTimeout(lastFunc)
      lastFunc = setTimeout(function () {
        if ((Date.now() - lastRan) >= limit) {
          func.apply(context, args)
          lastRan = Date.now()
        }
      }, limit - (Date.now() - lastRan))
    }
  }
}

/**
 *
 * @param {function} func
 * @param {number} delay - in milliseconds
 */
const debounce = (func, delay) => {
  let inDebounce
  return function () {
    const context = this
    const args = arguments
    clearTimeout(inDebounce)
    inDebounce = setTimeout(() => func.apply(context, args), delay)
  }
}

