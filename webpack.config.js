// var webpack = require('webpack')
const path = require('path')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

module.exports = {
  mode: 'production',
  entry: [
    'regenerator-runtime/runtime',
    './src/entry.js'
  ],
  watch: true,
  output: {
    path: path.resolve(__dirname, 'release'),
    filename: 'entrancetask.min.js',
    library: 'tasks',
    libraryTarget: 'umd'
  },
  externals: {
    three: {
      root: 'THREE',
      commonjs: 'three',
      commonjs2: 'three'
    },
    axios: 'axios'
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
  module: {
    rules: [
      {
        test: /\.(t|j)s$/,
        exclude: /node_modules/,
        use: { loader: 'awesome-typescript-loader' }
      }
    ]
  },
  plugins: [
    // new BundleAnalyzerPlugin({
    //   analyzerMode: 'static'
    // })
  ]
}
