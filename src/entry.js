let test = function () {
  console.log('test')
}
import Queue from '../helpfulskeleton/Queue'
import {
  Scene, PerspectiveCamera, WebGLRenderer, BoxGeometry, MeshBasicMaterial, Mesh, SphereBufferGeometry,
  Vector3, Sprite, OrthographicCamera
} from 'three'
import SkyBox from '../helpfulskeleton/SkyBox'

const RADIUS = 6378137.0
var scene = new Scene();
var camera = new PerspectiveCamera( 45, window.innerWidth/window.innerHeight, 0.1*RADIUS, 1000*RADIUS );

var renderer = new WebGLRenderer();
window.__renderer = renderer
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

var geometry = new SphereBufferGeometry( RADIUS, 32, 32 );
var material = new MeshBasicMaterial( { color: 0x00ff00 } );
var sphere = new Mesh( geometry, material );
scene.add( sphere );

camera.position.y = 3 * RADIUS;
camera.lookAt(new Vector3(0,0,0))

var animate = function () {
  requestAnimationFrame( animate );

  // sphere.rotation.x += 0.01;
  // sphere.rotation.y += 0.01;

  renderer.render( scene, camera );
};
animate()

let skybox = new SkyBox({parent: scene})
console.log(skybox)

export {
  test,
  Queue,
  animate
}
